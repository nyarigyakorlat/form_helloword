﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Backgroundworker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy) backgroundWorker1.RunWorkerAsync();
            button.Enabled = false;

            pBar1.Minimum = 1;
            pBar1.Maximum = 100;
            pBar1.Value = 1;
            pBar1.Step = 1;

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
            for (int i = 0; i <= 100; i++)
            {
                Dowork.Work();
                backgroundWorker1.ReportProgress(i, i);
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                
            }
            e.Result = true;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            label1.Text = e.ProgressPercentage.ToString();
            pBar1.PerformStep();

            int z=(int)e.UserState;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button.Enabled = true;
            if (e.Error != null) throw e.Error;
            if (e.Cancelled)
            {
                MessageBox.Show(this, "MEGSZAKITVA!");
                return;
            }
            if (e.Result != null)
            {
                bool result = (bool)e.Result;
                if (result) MessageBox.Show(this, "SIKER!");
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }
    }
}
