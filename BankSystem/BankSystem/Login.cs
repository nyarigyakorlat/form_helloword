﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using NLog;

namespace BankSystem
{
    public partial class Login : UserControl
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Login _instance;
        public static Login Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Login();
                return _instance;
            }
        }
        public Login()
        {
            InitializeComponent();
        }
        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                DataContainer.Instance.Load();
            }
            catch (Exception exc)
            {
                lbl_Hiba.Text = exc.Message;
            }
        }
        private void txb_Username_TextChanged(object sender, EventArgs e)
        {
            if (txb_Username.Text.Length > 4 && txb_Password.Text.Length > 4)
            {
                btn_Login.Enabled = true;
                btn_Register.Enabled = true;
            }
            else
            {
                btn_Register.Enabled = false;
                btn_Login.Enabled = false;
            }
        }
        private void txb_Password_TextChanged(object sender, EventArgs e)
        {
            if (txb_Username.Text.Length > 4 && txb_Password.Text.Length > 4)
            {
                btn_Login.Enabled = true;
                btn_Register.Enabled = true;
            }
            else
            {
                btn_Register.Enabled = false;
                btn_Login.Enabled = false;
            }
        }
        private void btn_Login_Click(object sender, EventArgs e)
        {
            try
            {
                BLogin();
                DataContainer.Instance.Save();
                BankSystem.Instance.Panelswich(BankSystem.Panel.Fooldal);
                
            }
            catch (Exception exc)
            {
                lbl_Hiba.Text = exc.Message;
            }
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            try
            {
                User user = new User(txb_Username.Text, txb_Password.Text, 0, 0);
                Register(user);
            }
            catch (Exception exc)
            {
                lbl_Hiba.Text = exc.Message;
            }
        }
        public void Register(User user)
        {
            foreach (var member in DataContainer.Instance.UsersList)
            {
                if (user.Felhasznalo == member.Felhasznalo)
                {
                    logger.Error("Sikertelen regisztráció.");
                    throw new Exception("Felhasználó foglalt!");
                }
            }
            DataContainer.Instance.UsersList.Add(user);
            logger.Info($"Sikeres regisztráció, {user.Felhasznalo} részére.");
        }
        public void BLogin()
        {
            foreach (var member in DataContainer.Instance.UsersList)
            {
                if (txb_Username.Text == member.Felhasznalo && txb_Password.Text == member.Password)
                {
                    logger.Info($"Sikeres bejelentkezés, {txb_Username.Text} részére.");
                    return;
                }
            }
            logger.Error("Hibás felhasználó név, vagy jelszó");
            throw new Exception("Hibás felhasználónév vagy jelszó!");
            
        }
        
    }

}
