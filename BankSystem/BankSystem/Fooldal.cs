﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystem
{
    public partial class Fooldal : UserControl
    {
        private static Fooldal _instance;
        public static Fooldal Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Fooldal();
                return _instance;
            }
        }
        public enum Panel
        {
            Fomenu, Hitel, Szamla, Helpdesk
        }
        public void Panelswich(Panel panel)
        {
            switch (panel)
            {
                case Panel.Fomenu:
                    if (!panel_Controls.Controls.Contains(UC_Fooldal.Instance))
                    {
                        panel_Controls.Controls.Add(UC_Fooldal.Instance);
                        UC_Fooldal.Instance.Dock = DockStyle.Fill;
                        UC_Fooldal.Instance.BringToFront();
                    }
                    else
                    {
                        UC_Fooldal.Instance.BringToFront();
                    }
                    break;
                case Panel.Hitel:
                    if (!panel_Controls.Controls.Contains(UC_Hitel.Instance))
                    {
                        panel_Controls.Controls.Add(UC_Hitel.Instance);
                        UC_Hitel.Instance.Dock = DockStyle.Fill;
                        UC_Hitel.Instance.BringToFront();
                    }
                    else
                    {
                        UC_Hitel.Instance.BringToFront();
                    }
                    break;
                case Panel.Szamla:
                    if (!panel_Controls.Controls.Contains(UC_Szamla.Instance))
                    {
                        panel_Controls.Controls.Add(UC_Szamla.Instance);
                        UC_Szamla.Instance.Dock = DockStyle.Fill;
                        UC_Szamla.Instance.BringToFront();
                    }
                    else
                    {
                        UC_Szamla.Instance.BringToFront();
                    }
                    break;
                case Panel.Helpdesk:
                    if (!panel_Controls.Controls.Contains(UC_Helpdesk.Instance))
                    {
                        panel_Controls.Controls.Add(UC_Helpdesk.Instance);
                        UC_Helpdesk.Instance.Dock = DockStyle.Fill;
                        UC_Helpdesk.Instance.BringToFront();
                    }
                    else
                    {
                        UC_Helpdesk.Instance.BringToFront();
                    }
                    break;
                default:
                    break;
            }
        }
        public Fooldal()
        {
            InitializeComponent();
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            BankSystem.Instance.Panelswich(BankSystem.Panel.Login);
        }

        private void btn_Fooldal_Click(object sender, EventArgs e)
        {
            Panelswich(Panel.Fomenu);
        }

        private void btn_Hitel_Click(object sender, EventArgs e)
        {
            Panelswich(Panel.Hitel);
        }

        private void btn_inform_Click(object sender, EventArgs e)
        {
            Panelswich(Panel.Szamla);
        }

        private void btn_Help_Click(object sender, EventArgs e)
        {
            Panelswich(Panel.Helpdesk);
        }

        private void Fooldal_Load(object sender, EventArgs e)
        {
            Panelswich(Panel.Fomenu);
        }
    }
}
