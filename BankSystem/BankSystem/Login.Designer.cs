﻿namespace BankSystem
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Fnev = new System.Windows.Forms.Label();
            this.lbl_Password = new System.Windows.Forms.Label();
            this.txb_Username = new System.Windows.Forms.TextBox();
            this.txb_Password = new System.Windows.Forms.TextBox();
            this.btn_Login = new System.Windows.Forms.Button();
            this.lbl_Hiba = new System.Windows.Forms.Label();
            this.btn_Register = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_Fnev
            // 
            this.lbl_Fnev.AutoSize = true;
            this.lbl_Fnev.Location = new System.Drawing.Point(259, 144);
            this.lbl_Fnev.Name = "lbl_Fnev";
            this.lbl_Fnev.Size = new System.Drawing.Size(84, 13);
            this.lbl_Fnev.TabIndex = 0;
            this.lbl_Fnev.Text = "Felhasználónév:";
            // 
            // lbl_Password
            // 
            this.lbl_Password.AutoSize = true;
            this.lbl_Password.Location = new System.Drawing.Point(304, 183);
            this.lbl_Password.Name = "lbl_Password";
            this.lbl_Password.Size = new System.Drawing.Size(39, 13);
            this.lbl_Password.TabIndex = 0;
            this.lbl_Password.Text = "Jelszó:";
            // 
            // txb_Username
            // 
            this.txb_Username.Location = new System.Drawing.Point(349, 141);
            this.txb_Username.Name = "txb_Username";
            this.txb_Username.Size = new System.Drawing.Size(100, 20);
            this.txb_Username.TabIndex = 1;
            this.txb_Username.TextChanged += new System.EventHandler(this.txb_Username_TextChanged);
            // 
            // txb_Password
            // 
            this.txb_Password.Location = new System.Drawing.Point(349, 180);
            this.txb_Password.Name = "txb_Password";
            this.txb_Password.PasswordChar = '*';
            this.txb_Password.Size = new System.Drawing.Size(100, 20);
            this.txb_Password.TabIndex = 2;
            this.txb_Password.TextChanged += new System.EventHandler(this.txb_Password_TextChanged);
            // 
            // btn_Login
            // 
            this.btn_Login.Enabled = false;
            this.btn_Login.Location = new System.Drawing.Point(501, 242);
            this.btn_Login.Name = "btn_Login";
            this.btn_Login.Size = new System.Drawing.Size(75, 23);
            this.btn_Login.TabIndex = 3;
            this.btn_Login.Text = "Login";
            this.btn_Login.UseVisualStyleBackColor = true;
            this.btn_Login.Click += new System.EventHandler(this.btn_Login_Click);
            // 
            // lbl_Hiba
            // 
            this.lbl_Hiba.AutoSize = true;
            this.lbl_Hiba.ForeColor = System.Drawing.Color.Red;
            this.lbl_Hiba.Location = new System.Drawing.Point(259, 214);
            this.lbl_Hiba.Name = "lbl_Hiba";
            this.lbl_Hiba.Size = new System.Drawing.Size(0, 13);
            this.lbl_Hiba.TabIndex = 5;
            // 
            // btn_Register
            // 
            this.btn_Register.Enabled = false;
            this.btn_Register.Location = new System.Drawing.Point(420, 242);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Size = new System.Drawing.Size(75, 23);
            this.btn_Register.TabIndex = 6;
            this.btn_Register.Text = "Regisztráció";
            this.btn_Register.UseVisualStyleBackColor = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.lbl_Hiba);
            this.Controls.Add(this.btn_Login);
            this.Controls.Add(this.txb_Password);
            this.Controls.Add(this.txb_Username);
            this.Controls.Add(this.lbl_Password);
            this.Controls.Add(this.lbl_Fnev);
            this.Name = "Login";
            this.Size = new System.Drawing.Size(801, 390);
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Fnev;
        private System.Windows.Forms.Label lbl_Password;
        private System.Windows.Forms.TextBox txb_Username;
        private System.Windows.Forms.TextBox txb_Password;
        private System.Windows.Forms.Button btn_Login;
        public System.Windows.Forms.Label lbl_Hiba;
        private System.Windows.Forms.Button btn_Register;
    }
}
