﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace BankSystem
{
    [DataContract]
    public class DataContainer
    {
        private static DataContainer _instance;
        public static DataContainer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DataContainer();
                return _instance;
            }
        }
        [DataMember]
        public List<User> UsersList = new List<User>();

        public void Save()
        {
            DataContractSerializer ds = new DataContractSerializer(typeof(DataContainer));
            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (XmlWriter w = XmlWriter.Create("datacontainer.xml", settings)) ds.WriteObject(w, Instance);
        }
        public void Load()
        {
            DataContractSerializer ds = new DataContractSerializer(typeof(DataContainer));
            using (Stream s = File.OpenRead("datacontainer.xml"))
                _instance = (DataContainer)ds.ReadObject(s);
        }
        protected DataContainer()
        {
            
        }
    }
    [DataContract]
    public class User
    {
        public User(string nev, string jelszo, int egyenleg, int hitel)
        {
            Password = jelszo;
            Felhasznalo = nev;
            this.hitel = hitel;
            this.egyenleg = egyenleg;
        }
        [DataMember]
        public int egyenleg, hitel;
        [DataMember]
        private string _felhasznalo;
        public string Felhasznalo
        {
            get
            {
                return _felhasznalo;
            }
            set
            {
                _felhasznalo = value;
            }
        }
        [DataMember]
        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }
    }
}