﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystem
{
    public partial class UC_Helpdesk : UserControl
    {
        private static UC_Helpdesk _instance;
        public static UC_Helpdesk Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Helpdesk();
                return _instance;
            }
        }
        public UC_Helpdesk()
        {
            InitializeComponent();
        }
    }
}
