﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystem
{
    public partial class UC_Hitel : UserControl
    {
        private static UC_Hitel _instance;
        public static UC_Hitel Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Hitel();
                return _instance;
            }
        }
        public UC_Hitel()
        {
            InitializeComponent();
        }
    }
}
