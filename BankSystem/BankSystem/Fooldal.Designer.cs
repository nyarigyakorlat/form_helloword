﻿namespace BankSystem
{
    partial class Fooldal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Logout = new System.Windows.Forms.Button();
            this.btn_Help = new System.Windows.Forms.Button();
            this.btn_inform = new System.Windows.Forms.Button();
            this.btn_Hitel = new System.Windows.Forms.Button();
            this.btn_Fooldal = new System.Windows.Forms.Button();
            this.panel_Controls = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(117, 390);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.btn_Logout, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btn_Help, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn_inform, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_Hitel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_Fooldal, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(117, 390);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btn_Logout
            // 
            this.btn_Logout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Logout.Location = new System.Drawing.Point(3, 155);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(111, 32);
            this.btn_Logout.TabIndex = 4;
            this.btn_Logout.Text = "Kijelentkezés";
            this.btn_Logout.UseVisualStyleBackColor = true;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // btn_Help
            // 
            this.btn_Help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Help.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Help.Location = new System.Drawing.Point(3, 117);
            this.btn_Help.Name = "btn_Help";
            this.btn_Help.Size = new System.Drawing.Size(111, 32);
            this.btn_Help.TabIndex = 3;
            this.btn_Help.Text = "Helpdesk";
            this.btn_Help.UseVisualStyleBackColor = true;
            this.btn_Help.Click += new System.EventHandler(this.btn_Help_Click);
            // 
            // btn_inform
            // 
            this.btn_inform.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_inform.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_inform.Location = new System.Drawing.Point(3, 79);
            this.btn_inform.Name = "btn_inform";
            this.btn_inform.Size = new System.Drawing.Size(111, 32);
            this.btn_inform.TabIndex = 2;
            this.btn_inform.Text = "Számla információk";
            this.btn_inform.UseVisualStyleBackColor = true;
            this.btn_inform.Click += new System.EventHandler(this.btn_inform_Click);
            // 
            // btn_Hitel
            // 
            this.btn_Hitel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Hitel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Hitel.Location = new System.Drawing.Point(3, 41);
            this.btn_Hitel.Name = "btn_Hitel";
            this.btn_Hitel.Size = new System.Drawing.Size(111, 32);
            this.btn_Hitel.TabIndex = 1;
            this.btn_Hitel.Text = "Hitel";
            this.btn_Hitel.UseVisualStyleBackColor = true;
            this.btn_Hitel.Click += new System.EventHandler(this.btn_Hitel_Click);
            // 
            // btn_Fooldal
            // 
            this.btn_Fooldal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Fooldal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Fooldal.Location = new System.Drawing.Point(3, 3);
            this.btn_Fooldal.Name = "btn_Fooldal";
            this.btn_Fooldal.Size = new System.Drawing.Size(111, 32);
            this.btn_Fooldal.TabIndex = 0;
            this.btn_Fooldal.Text = "Főoldal";
            this.btn_Fooldal.UseVisualStyleBackColor = true;
            this.btn_Fooldal.Click += new System.EventHandler(this.btn_Fooldal_Click);
            // 
            // panel_Controls
            // 
            this.panel_Controls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Controls.Location = new System.Drawing.Point(117, 0);
            this.panel_Controls.Name = "panel_Controls";
            this.panel_Controls.Size = new System.Drawing.Size(684, 390);
            this.panel_Controls.TabIndex = 1;
            // 
            // Fooldal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel_Controls);
            this.Controls.Add(this.panel1);
            this.Name = "Fooldal";
            this.Size = new System.Drawing.Size(801, 390);
            this.Load += new System.EventHandler(this.Fooldal_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.Button btn_Help;
        private System.Windows.Forms.Button btn_inform;
        private System.Windows.Forms.Button btn_Hitel;
        private System.Windows.Forms.Button btn_Fooldal;
        private System.Windows.Forms.Panel panel_Controls;
    }
}
