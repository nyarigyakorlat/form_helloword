﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystem
{
    public partial class BankSystem : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static BankSystem _instance;
        public static BankSystem Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new BankSystem();
                }
                return _instance;
            }
        }
        public enum Panel { Login , Fooldal }
        public void Panelswich(Panel panel)
        {
            switch (panel)
            {
                case Panel.Login:
                    if (!pnl_default.Controls.Contains(Login.Instance))
                    {
                        pnl_default.Controls.Add(Login.Instance);
                        Login.Instance.Dock = DockStyle.Fill;
                        Login.Instance.BringToFront();
                    }
                    else
                    {
                        Login.Instance.BringToFront();
                    }
                    break;
                case Panel.Fooldal:
                    if (!pnl_default.Controls.Contains(Fooldal.Instance))
                    {
                        pnl_default.Controls.Add(Fooldal.Instance);
                        Fooldal.Instance.Dock = DockStyle.Fill;
                        Fooldal.Instance.BringToFront();
                    }
                    else
                    {
                        Fooldal.Instance.BringToFront();
                    }
                    break;
                default:
                    break;
            }
        }
        protected BankSystem()
        {
            InitializeComponent();
        }

        private void BankSystem_Shown(object sender, EventArgs e)
        {
           logger.Info("Alkalmazás elindult");

            Panelswich(Panel.Login);
        }

        private void BankSystem_FormClosed(object sender, FormClosedEventArgs e)
        {
            logger.Info("Alkalmazás leállt");
        }
    }
}
