﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystem
{
    public partial class UC_Szamla : UserControl
    {
        private static UC_Szamla _instance;
        public static UC_Szamla Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Szamla();
                return _instance;
            }
        }
        public UC_Szamla()
        {
            InitializeComponent();
        }
    }
}
