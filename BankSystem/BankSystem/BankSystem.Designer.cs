﻿namespace BankSystem
{
    partial class BankSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_default = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnl_default
            // 
            this.pnl_default.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_default.Location = new System.Drawing.Point(0, 0);
            this.pnl_default.Name = "pnl_default";
            this.pnl_default.Size = new System.Drawing.Size(801, 390);
            this.pnl_default.TabIndex = 0;
            // 
            // BankSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 390);
            this.Controls.Add(this.pnl_default);
            this.Name = "BankSystem";
            this.Text = "Bank System";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BankSystem_FormClosed);
            this.Shown += new System.EventHandler(this.BankSystem_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_default;
    }
}

