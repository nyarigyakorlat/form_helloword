﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankSystem
{
    public partial class UC_Fooldal : UserControl
    {
        private static UC_Fooldal _instance;
        public static UC_Fooldal Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Fooldal();
                return _instance;
            }
        }
        protected UC_Fooldal()
        {
            InitializeComponent();
            lbl_udvozloszoveg.Text = $"Üdvözöljük a BankSystem Bank Alkalmazásában";
        }
    }
}
