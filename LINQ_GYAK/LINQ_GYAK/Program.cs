﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_GYAK
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> Felhasznalok = new List<User>();
            Felhasznalok.Add(new User("Andras", Sex.Férfi, 25));
            Felhasznalok.Add(new User("Kriszti", Sex.Nő, 19));
            Felhasznalok.Add(new User("Zsombor", Sex.Férfi, 39));
            Felhasznalok.Add(new User("Dominik", Sex.Férfi, 22));
            Felhasznalok.Add(new User("Andrea", Sex.Nő, 21));

            List<User> Oreg = new List<User>();
            List<User> Ferfi = new List<User>();
            foreach (var user in Felhasznalok)
            {
                if (user.Kor > 25) Oreg.Add(user);
                if (user.Nem == Sex.Férfi) Ferfi.Add(user);
            }
            Console.WriteLine("Férfiak: ");
            foreach (var ferfi in Ferfi)
            {
                ferfi.Paint();
            }
            Console.WriteLine("Öregek: ");
            foreach (var ore in Oreg)
            {
                ore.Paint();
            }

            var ferfi2 = Felhasznalok
                .Where(ur => ur.Nem == Sex.Férfi)
                .Where(ur => ur.Kor>25)
                .OrderBy(ur => ur.Kor)
                .ToList();
            
            Console.ReadKey();
        }
    }
    enum Sex
    {
        Nő, Férfi
    }
    class User
    {

        public User(string name,Sex sex, int kor)
        {
            Name = name;
            Kor = kor;
            switch (sex)
            {
                case Sex.Nő:
                    Nem = Sex.Nő;
                    break;
                case Sex.Férfi:
                    Nem = Sex.Férfi;
                    break;
                default:
                    break;
            }
        }
        public void Paint()
        {
            Console.WriteLine($"{Name} {Nem} {Kor}");
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        private int _kor;
        public int Kor
        {
            get
            {
                return _kor;
            }
            set
            {
              if (value > 0 && value < 120)  _kor = value;
            }
        }
        private Sex _nem;
        public Sex Nem
        {
            get
            {
                return _nem;
            }
            set
            {
                if (value == Sex.Férfi || value == Sex.Nő) _nem = value;
            }
        }
            //kor f/n enum név Lista 5 user
    }
}
