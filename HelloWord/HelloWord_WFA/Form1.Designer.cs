﻿namespace HelloWord_WFA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_Hozzaad = new System.Windows.Forms.Button();
            this.btn_Torles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 41);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(447, 325);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btn_Hozzaad
            // 
            this.btn_Hozzaad.Location = new System.Drawing.Point(12, 12);
            this.btn_Hozzaad.Name = "btn_Hozzaad";
            this.btn_Hozzaad.Size = new System.Drawing.Size(75, 23);
            this.btn_Hozzaad.TabIndex = 3;
            this.btn_Hozzaad.Text = "Hozzáad";
            this.btn_Hozzaad.UseVisualStyleBackColor = true;
            this.btn_Hozzaad.Click += new System.EventHandler(this.btn_Hozzaad_Click);
            // 
            // btn_Torles
            // 
            this.btn_Torles.Location = new System.Drawing.Point(93, 12);
            this.btn_Torles.Name = "btn_Torles";
            this.btn_Torles.Size = new System.Drawing.Size(75, 23);
            this.btn_Torles.TabIndex = 3;
            this.btn_Torles.Text = "Törlés";
            this.btn_Torles.UseVisualStyleBackColor = true;
            this.btn_Torles.Click += new System.EventHandler(this.btn_Torles_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 378);
            this.Controls.Add(this.btn_Torles);
            this.Controls.Add(this.btn_Hozzaad);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btn_Hozzaad;
        private System.Windows.Forms.Button btn_Torles;
    }
}

