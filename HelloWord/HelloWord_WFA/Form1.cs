﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloWord_WFA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void btn_Hozzaad_Click(object sender, EventArgs e)
        {
            Button button = new Button();
            button.Click += Button_Click;
            button.Text = "Gomb " + (flowLayoutPanel1.Controls.Count + 1);
            flowLayoutPanel1.Controls.Add(button);
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            object o = b;
            MessageBox.Show(this, $"Megnyomtad a {b.Text} gombot.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void btn_Torles_Click(object sender, EventArgs e)
        {
            if (flowLayoutPanel1.Controls.Count == 0) return;

            flowLayoutPanel1.Controls.RemoveAt(flowLayoutPanel1.Controls.Count-1);
        }
    }
}
