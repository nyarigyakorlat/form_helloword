﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Urlap
{
  
    public partial class Urlap : Form
    {
        public Urlap()
        {
            InitializeComponent();
        }

        private void btn_Sub_Click(object sender, EventArgs e)
        {
            try
            {
                string sex= "";
                if (rbt_Male.Checked) sex = "Férfi";
                else if (rbt_Female.Checked) sex = "Nő";
                else if (rbt_Extra.Checked) sex = "Egyéb";
                Person Member = new Person(txb_Fname.Text + " " + txb_Lname.Text, dtp_Borndate.Value, sex);
            }
            catch (Exception exc)
            {
               var dg = MessageBox.Show(this,"Hiba történt : " + exc.Message, "ERROR", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                if (!(dg == DialogResult.OK)) Application.Exit();


            }
        }
    }
}
