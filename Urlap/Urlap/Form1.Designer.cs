﻿namespace Urlap
{
    partial class Urlap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Name = new System.Windows.Forms.Label();
            this.lbl_keresztnev = new System.Windows.Forms.Label();
            this.txb_Fname = new System.Windows.Forms.TextBox();
            this.txb_Lname = new System.Windows.Forms.TextBox();
            this.lbl_Date = new System.Windows.Forms.Label();
            this.dtp_Borndate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.rbt_Male = new System.Windows.Forms.RadioButton();
            this.rbt_Female = new System.Windows.Forms.RadioButton();
            this.rbt_Extra = new System.Windows.Forms.RadioButton();
            this.btn_Sub = new System.Windows.Forms.Button();
            this.labelVezeteknevHiba = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(48, 35);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(70, 13);
            this.lbl_Name.TabIndex = 0;
            this.lbl_Name.Text = "Vezetéknév: ";
            // 
            // lbl_keresztnev
            // 
            this.lbl_keresztnev.AutoSize = true;
            this.lbl_keresztnev.Location = new System.Drawing.Point(230, 35);
            this.lbl_keresztnev.Name = "lbl_keresztnev";
            this.lbl_keresztnev.Size = new System.Drawing.Size(63, 13);
            this.lbl_keresztnev.TabIndex = 0;
            this.lbl_keresztnev.Text = "Keresztnév:";
            // 
            // txb_Fname
            // 
            this.txb_Fname.Location = new System.Drawing.Point(124, 32);
            this.txb_Fname.Name = "txb_Fname";
            this.txb_Fname.Size = new System.Drawing.Size(100, 20);
            this.txb_Fname.TabIndex = 1;
            // 
            // txb_Lname
            // 
            this.txb_Lname.Location = new System.Drawing.Point(299, 32);
            this.txb_Lname.Name = "txb_Lname";
            this.txb_Lname.Size = new System.Drawing.Size(100, 20);
            this.txb_Lname.TabIndex = 1;
            // 
            // lbl_Date
            // 
            this.lbl_Date.AutoSize = true;
            this.lbl_Date.Location = new System.Drawing.Point(48, 77);
            this.lbl_Date.Name = "lbl_Date";
            this.lbl_Date.Size = new System.Drawing.Size(85, 13);
            this.lbl_Date.TabIndex = 2;
            this.lbl_Date.Text = "Születés dátum: ";
            // 
            // dtp_Borndate
            // 
            this.dtp_Borndate.Location = new System.Drawing.Point(139, 73);
            this.dtp_Borndate.Name = "dtp_Borndate";
            this.dtp_Borndate.Size = new System.Drawing.Size(200, 20);
            this.dtp_Borndate.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Neme:";
            // 
            // rbt_Male
            // 
            this.rbt_Male.AutoSize = true;
            this.rbt_Male.Location = new System.Drawing.Point(94, 116);
            this.rbt_Male.Name = "rbt_Male";
            this.rbt_Male.Size = new System.Drawing.Size(45, 17);
            this.rbt_Male.TabIndex = 5;
            this.rbt_Male.TabStop = true;
            this.rbt_Male.Text = "Férfi";
            this.rbt_Male.UseVisualStyleBackColor = true;
            // 
            // rbt_Female
            // 
            this.rbt_Female.AutoSize = true;
            this.rbt_Female.Location = new System.Drawing.Point(145, 116);
            this.rbt_Female.Name = "rbt_Female";
            this.rbt_Female.Size = new System.Drawing.Size(39, 17);
            this.rbt_Female.TabIndex = 5;
            this.rbt_Female.TabStop = true;
            this.rbt_Female.Text = "Nő";
            this.rbt_Female.UseVisualStyleBackColor = true;
            // 
            // rbt_Extra
            // 
            this.rbt_Extra.AutoSize = true;
            this.rbt_Extra.Location = new System.Drawing.Point(190, 116);
            this.rbt_Extra.Name = "rbt_Extra";
            this.rbt_Extra.Size = new System.Drawing.Size(55, 17);
            this.rbt_Extra.TabIndex = 5;
            this.rbt_Extra.TabStop = true;
            this.rbt_Extra.Text = "Egyéb";
            this.rbt_Extra.UseVisualStyleBackColor = true;
            // 
            // btn_Sub
            // 
            this.btn_Sub.Location = new System.Drawing.Point(287, 161);
            this.btn_Sub.Name = "btn_Sub";
            this.btn_Sub.Size = new System.Drawing.Size(75, 23);
            this.btn_Sub.TabIndex = 6;
            this.btn_Sub.Text = "Submit";
            this.btn_Sub.UseVisualStyleBackColor = true;
            this.btn_Sub.Click += new System.EventHandler(this.btn_Sub_Click);
            // 
            // labelVezeteknevHiba
            // 
            this.labelVezeteknevHiba.AutoSize = true;
            this.labelVezeteknevHiba.ForeColor = System.Drawing.Color.Red;
            this.labelVezeteknevHiba.Location = new System.Drawing.Point(139, 54);
            this.labelVezeteknevHiba.Name = "labelVezeteknevHiba";
            this.labelVezeteknevHiba.Size = new System.Drawing.Size(0, 13);
            this.labelVezeteknevHiba.TabIndex = 7;
            this.labelVezeteknevHiba.Visible = false;
            // 
            // Urlap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 204);
            this.Controls.Add(this.labelVezeteknevHiba);
            this.Controls.Add(this.btn_Sub);
            this.Controls.Add(this.rbt_Extra);
            this.Controls.Add(this.rbt_Female);
            this.Controls.Add(this.rbt_Male);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtp_Borndate);
            this.Controls.Add(this.lbl_Date);
            this.Controls.Add(this.txb_Lname);
            this.Controls.Add(this.txb_Fname);
            this.Controls.Add(this.lbl_keresztnev);
            this.Controls.Add(this.lbl_Name);
            this.Name = "Urlap";
            this.Text = "Adatok felvétele";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Label lbl_keresztnev;
        private System.Windows.Forms.TextBox txb_Fname;
        private System.Windows.Forms.TextBox txb_Lname;
        private System.Windows.Forms.Label lbl_Date;
        private System.Windows.Forms.DateTimePicker dtp_Borndate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbt_Male;
        private System.Windows.Forms.RadioButton rbt_Female;
        private System.Windows.Forms.RadioButton rbt_Extra;
        private System.Windows.Forms.Button btn_Sub;
        private System.Windows.Forms.Label labelVezeteknevHiba;
    }
}

