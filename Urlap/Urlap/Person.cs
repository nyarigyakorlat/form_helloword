﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urlap
{
    class Person
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 7) throw new Exception("Hibás név.");
                _name = value;
            }
        }
        private DateTime _born;
        public DateTime Born
        {
            get
            {
                return _born;
            }
            set
            {
                DateTime min = new DateTime(1900, 01, 01);
                DateTime max = DateTime.Now;
                if (!(min < value && max > value)) throw new ArgumentOutOfRangeException("Hibás Születési dátum.");
                _born = value;
            }
        }
        private string _sex;
        public string Sex
        {
            get
            {
                return _sex;
            }
            set
            {
                if (value == "Férfi" || value == "Nő" || value == "Egyéb") _sex = value;
                else
                {
                    throw new Exception("Hibás \"Nem\".");
                }
            }
        }
        public Person(string name, DateTime born, string sex)
        {
            Name = name;
            Born = born;
            Sex = sex;
        }
    }
}
