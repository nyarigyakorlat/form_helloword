﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hibakezeles
{
    class Employ
    {

        public Employ(string name, DateTime born, string email)
        {
            Name = name;
            Born = born;
            Email = email;

        }
        private string _name = "";
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                string[] nev = value.Split(' ');
                if (nev.Length > 4) return;
                for (int i = 0; i < nev.Length; i++)
                {
                    nev[i] = nev[i].First().ToString().ToUpper() + nev[i].Substring(1);
                }
                _name = string.Join(" ", nev);
            }
        }
        private DateTime _born;
        public DateTime Born
        {
            get
            {
                return _born;
            }
            set
            {
                DateTime min = new DateTime(1900, 01, 01);
                DateTime max = DateTime.Now;
                if (!(min < value && max > value)) return;
                _born = value;
            }
        }
        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                string mail = value;
                if (!mail.Contains("@") || !mail.Contains(".")) throw new FormatException("Nincs @.");
                string[] tomb = mail.Split('@');
                if (tomb.Length != 2) throw new Exception("Rossz formátum.");
                if (tomb[0].Length < 3 && tomb[0].Length > 18) throw new Exception("Email cím hosszúsága nem megfelelő.");
                mail = tomb[1];
                string[] domain = mail.Split('.');
                if (domain.Length != 2) throw new Exception("Hibás Domain formátum.");
                if (domain[0].Length < 3 && domain[0].Length > 18) throw new Exception("Hibás domain.");
                mail = domain[1];
                if (mail.Length < 1 && mail.Length > 5) throw new Exception("Hibás régió jelzés.");



                _email = value;
            }
        }
        public void Paint()
        {
            Console.WriteLine("Neve: " + Name);
            Console.WriteLine("Születési ideje: " + Born.ToString("yyyy. MM. dd."));
            Console.WriteLine("E-Mail címe: " + Email);
        }
    //név dátum email    
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérem adja meg a nevét: ");
            string name = Console.ReadLine();
            Console.WriteLine("Kérem adja meg a születési dátumát (ÉÉÉÉ.HH.NN.): ");
            string date = Console.ReadLine();

            try
            {
                Employ Feri = new Employ("csárdás ferenc", new DateTime(2000, 03, 10), "csardas.f.licloud.com");
                Feri.Paint();

            }
            catch (FormatException fex)
            {
                Console.WriteLine("Formátum hiba:" + fex.Message);
            }
            catch (Exception exc)
            {
                Console.WriteLine("Hiba történt : " + exc.Message);

            }
            Console.ReadKey();

        }
    }
}
